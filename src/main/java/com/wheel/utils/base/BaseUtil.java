package com.wheel.utils.base;

import java.lang.reflect.InvocationTargetException;

/**
 * @author shenshu
 * @version 1.0
 * @description: 基本工具
 * @E-mail 254602848@qq.com
 * @date 2021/9/6 9:43
 */
public class BaseUtil {


    private BaseUtil() {
    }

    /**
     * 实列话泛型
     * @param tClass
     * @param <T>
     * @return
     */
    public static <T> T newInstance(Class<T> tClass) {
        T ret;

        try {
            ret = tClass.getDeclaredConstructor().newInstance();
        } catch (Exception e) {
            throw  new RuntimeException(e);
        }
        return ret;
    }
}
