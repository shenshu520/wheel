package com.wheel.utils.tree;

import com.wheel.utils.collection.CollectionUtil;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

/**
 * @description: 树工具
 * @author shenshu
 * @version 1.0
 * @E-mail 254602848@qq.com
 * @date 2021/9/3 12:56
 */
public class TreeUtil {


    private TreeUtil() {
    }


    /**
     * @description: 集合转树结构（非递归生成）
     * @param list        树数据集合
     * @param rootNodeId  根节点Id
     * @param getNodeId   获取节点Id处理
     * @param getNodePId  获取父节点Id处理
     * @param getNodeSons 获取子节点处理
     * @author shenshu
     * @date: 2021/9/3 13:22
     * T    节点对象
     * K    节点Id类型
     * @return: java.util.List<T>
     */
    public static <T, K> List<T> toTree(List<T> list, K rootNodeId, Function<T, K> getNodeId, Function<T, K> getNodePId, Function<T, List<T>> getNodeSons) {

        if (list == null) {
            return null;
        }

        Map<K, T> nodeIdMap = new LinkedHashMap<>();
        Map<K, List<T>> nodePIdMap = new LinkedHashMap<>();

        //映射生成
        for (T t : list) {
            //节点ID映射生成
            K k1 = getNodeId.apply(t);
            nodeIdMap.put(k1, t);

            //父节点Id映射生成
            K k2 = getNodePId.apply(t);
            if (nodePIdMap.containsKey(k2)) {
                List<T> mapList = nodePIdMap.get(k2);
                mapList.add(t);
            } else {
                List<T> mapList = new ArrayList<>();
                mapList.add(t);
                nodePIdMap.put(k2, mapList);
            }
        }

        //生成树结构
        for (Map.Entry<K, T> ktEntry : nodeIdMap.entrySet()) {
            K key = ktEntry.getKey();
            T t = ktEntry.getValue();

            List<T> iTrees = getNodeSons.apply(t);
            List<T> ts = nodePIdMap.get(key);
            if (ts != null) {
                iTrees.addAll(ts);
            }

        }

        return nodePIdMap.get(rootNodeId);
    }
}
