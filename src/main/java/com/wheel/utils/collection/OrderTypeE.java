package com.wheel.utils.collection;

/**
 * @description: 排序类型
 * @author shenshu
 * @version 1.0
 * @E-mail 254602848@qq.com
 * @date 2021/9/6 14:51
 */
public enum OrderTypeE {
    /**
     * @description: 升序
     */
    ASC,
    /**
     * @description: 降序
     */
    DESC,
}
