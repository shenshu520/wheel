package com.wheel.utils.collection;



import com.wheel.utils.base.BaseUtil;

import java.util.*;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * @description: 集合工具
 * @author shenshu
 * @version 1.0
 * @E-mail 254602848@qq.com
 * @date 2021/9/3 11:22
 */
public class CollectionUtil {


    private CollectionUtil() {
    }

    /**
     * @description: 转属性集合
     * @param collection  实体集合
     * @param getProperty 属性处理接口
     *                    E    实体
     *                    P    属性类型
     *                    R    返回类型
     * @author shenshu
     * @date: 2021/9/3 11:44
     * @return: 参数collection类型
     */
    public static <E, P> Collection<P> toProperty(Collection<E> collection, Function<E, P> getProperty) {
        Collection<P> ret = BaseUtil.newInstance(collection.getClass());
        for (E e : collection) {
            ret.add(getProperty.apply(e));
        }
        return ret;
    }




    /**
     * @description: 转Map，相同K不会处理
     * @param collection 数据集合
     * @param getKey     Key处理接口
     * @author shenshu
     * @date: 2021/9/3 12:40
     * @return: LinkedHashMap<K, E>
     */
    public static <E, K> Map<K, E> toMap(Collection<E> collection, Function<E, K> getKey) {
        LinkedHashMap<K,E> ret=new LinkedHashMap();
        for (E e : collection) {
            K k = getKey.apply(e);
            ret.putIfAbsent(k, e);
        }
        return ret;
    }




    /**
     * @description: 转Map，相同Key 加入集合中
     * @param collection 数据集合
     * @param getKey     Key处理接口
     * @author shenshu
     * @date: 2021/9/3 12:40
     * @return: LinkedHashMap<K, E>
     */
    public static <E, K> Map<K,List<E>> toMapList(Collection<E> collection, Function<E, K> getKey) {

        LinkedHashMap<K,List<E>> ret=new LinkedHashMap();
        for (E e : collection) {
            K k = getKey.apply(e);
            if (ret.containsKey(k)) {
                List<E> mapList = ret.get(k);
                mapList.add(e);
            } else {
                List<E> mapList = new ArrayList<>();
                mapList.add(e);
                ret.put(k, mapList);
            }
        }
        return ret;
    }




    /**
     * @description: 过滤集合数据
     * @param collection 集合数据
     * @param filter     过滤条件处理器
     * @author shenshu
     * @date: 2021/9/6 11:17
     * @return: 参数collection类型
     */
    public static <T> Collection<T> filter(Collection<T> collection, Predicate<T> filter) {
        Collection ret = BaseUtil.newInstance(collection.getClass());
        for (T t : collection) {
            if (!filter.test(t)) {
                ret.add(t);
            }
        }
        return ret;
    }


    /**
     * @description: 去重
     * @param collection 集合
     * @param getKey     去重数据
     * @author shenshu
     * @date: 2021/9/7 15:38
     * @return: 参数collection类型
     */
    public static <T> Collection<T> distinct(Collection<T> collection, Function<? super T, ?> getKey) {
        Collection ret = BaseUtil.newInstance(collection.getClass());

        LinkedHashMap<Object, T> map = new LinkedHashMap<>();
        for (T t : collection) {
            map.putIfAbsent(getKey.apply(t), t);
        }
        Collection<T> values = map.values();
        ret.addAll(values);
        return ret;
    }


    /**
     * @description: 去重
     * @param collection 集合
     * @author shenshu
     * @date: 2021/9/7 15:53
     * @return: 参数collection类型
     */
    public static <T> Collection<T> distinct(Collection<T> collection) {
        return distinct(collection, a -> a);
    }


    /**
     * @description: List排序
     * @param list       需要排序的集合
     * @param getValue   排序的数据
     * @param orderTypeE 排序类型
     * @author shenshu
     * @date: 2021/9/6 14:45
     * @return: 参数list类型
     */
    public static <T, V extends Comparable> List<T> orderBy(List<T> list, Function<T, V> getValue, OrderTypeE orderTypeE) {

        Collections.sort(list, (a, b) -> {

            int ret = 0;

            V aV = getValue.apply(a);
            V bV = getValue.apply(b);

            if (orderTypeE.equals(OrderTypeE.ASC)) {
                ret = aV.compareTo(bV);
            }

            if (orderTypeE.equals(OrderTypeE.DESC)) {
                ret = bV.compareTo(aV);
            }

            return ret;
        });
        return list;

    }

    /**
     * @description: List排序
     * @param list       需要排序的集合
     * @param orderTypeE 排序类型
     * @author shenshu
     * @date: 2021/9/6 15:11
     * @return: java.util.List<T>
     */
    public static <T extends Comparable> List<T> orderBy(List<T> list, OrderTypeE orderTypeE) {
        return orderBy(list, (e) -> e, orderTypeE);
    }


}
