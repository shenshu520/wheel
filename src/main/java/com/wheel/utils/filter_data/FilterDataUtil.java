package com.wheel.utils.filter_data;

import com.wheel.custom.exception.BizException;
import com.wheel.custom.result.Result;
import org.jetbrains.annotations.NotNull;


/**
 * @author shenshu
 * @version 1.0
 * @description: 过滤数据工具
 * @E-mail 254602848@qq.com
 * @date 2021/9/3 15:19
 */
public class FilterDataUtil {

    private FilterDataUtil() {

    }


    /**
     * @description: 过滤null数据。 null 抛异常
     * @param data  数据
     * @param runtimeException  异常
     * @author shenshu
     * @date: 2021/9/3 15:51
     * @return: T
     */
    public static <T, E extends RuntimeException> T filterNull(T data, E runtimeException) {
        if (data == null) {
            throw runtimeException;
        }
        return data;
    }

    /**
     * @description: 过滤null数据。 null 抛异业务常
     * @author shenshu
     * @date: 2021/9/26 16:03
     * @param data 数据
     * @param bizException   业务异常
     * @return: 数据
     */
    public static <T, E extends RuntimeException> T filterBizNull(T data, BizException bizException) {
        return filterNull(data,bizException);
    }


    /** 
     * @description:  过滤null数据。 null 抛异常
     * @author shenshu
     * @date: 2021/9/26 16:04 
     * @param data 数据
     * @param msg   错误消息
     * @return: 数据
     */ 
    public static <T, E extends RuntimeException> T filterBizNull(T data, String msg) {
        return filterNull(data,new BizException(msg, Result.CODE_FAIL));
    }


    /**
     * @description: 过滤字符串，移除左右空格。null || "" 抛异常
     * @param data  数据
     * @param runtimeException  异常
     * @author shenshu
     * @date: 2021/9/3 15:48
     * @return: java.lang.String
     */
    public static <E extends RuntimeException> String filterEmpty(String data, E runtimeException) {
        data = filterNull(data, runtimeException);
        data = data.trim();
        if (data.equals("")) {
            throw runtimeException;
        }
        return data;
    }
    
    /** 
     * @description:
     * @author shenshu
     * @date: 2021/9/26 16:05 
     * @param data 数据
     * @param bizException 业务异常
     * @return: java.lang.String
     */ 
    public static String filterBizEmpty(String data, BizException bizException) {
        return filterEmpty(data,bizException);
    }

    /**
     * @description:
     * @author shenshu
     * @date: 2021/9/26 16:06
     * @param data 数据
     * @param msg   异常消息
     * @return: java.lang.String
     */
    public static String filterBizEmpty(String data, String msg) {
        return filterEmpty(data,new BizException(msg, Result.CODE_FAIL));
    }


    /**
     * @description: 过滤数据。null || 0 抛异常
     * @param data  数据
     * @param runtimeException  异常
     * @author shenshu
     * @date: 2021/9/3 15:48
     * @return: java.lang.String
     */
    public static <E extends RuntimeException> Integer filterZero(Integer data, E runtimeException) {
        filterNull(data, runtimeException);
        if (data.equals(0)) {
            throw runtimeException;
        }
        return data;
    }


    public static Integer filterBizZero(Integer data, BizException bizException) {
        return filterZero(data,bizException);
    }



    public static Integer filterBizZero(Integer data, String msg) {
        return filterZero(data,new BizException(msg, Result.CODE_FAIL));
    }


    /**
     * @description: 过滤数据。null || 0 抛异常
     * @param data  数据
     * @param runtimeException  异常
     * @author shenshu
     * @date: 2021/9/3 15:48
     * @return: java.lang.String
     */
    public static <E extends RuntimeException> Long filterZero(Long data, E runtimeException) {
        filterNull(data, runtimeException);
        if (data.equals(0L)) {
            throw runtimeException;
        }
        return data;
    }




    public static Long filterBizZero(Long data, BizException bizException) {
        return filterZero(data,bizException);
    }


    public static Long filterBizZero(Long data, String msg) {
        return filterZero(data,new BizException(msg, Result.CODE_FAIL));
    }



    /**
     * @description: 过滤null数据 并且自定义过滤条件  null || custom==true 抛异常
     * @param data             需要过滤的数据
     * @param custom           自定义过滤条件
     * @param runtimeException 异常
     * @author shenshu
     * @date: 2021/9/3 15:47
     * @return: T
     */
    public static <T, E extends RuntimeException> T filterNullCustom(T data, boolean custom, E runtimeException) {
        filterNull(data, runtimeException);
        if (custom) {
            throw runtimeException;
        }
        return data;
    }


}
