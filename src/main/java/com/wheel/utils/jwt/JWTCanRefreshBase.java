package com.wheel.utils.jwt;

import com.auth0.jwt.interfaces.DecodedJWT;
import com.wheel.utils.jwt.entity.JWTDecoderNewTokenBase;

import java.util.HashMap;
import java.util.Map;

/**
 * 能会刷新的JWB
 */
public class JWTCanRefreshBase{

    /**
     * JWT区域名
     */
    private String zoneName;
    /**
     * 秘钥
     */
    private String secret = null;
    /**
     * 过期时间
     */
    private int expiredSecond = 0;

    /**
     * 剩余多少秒后刷新 (过期时间剩余多少秒刷新。不推荐设置太长)
     */
    private int remainSecondRefresh;

    /**
     * JWT 区域 Map
     */
    private static Map<String, JWTCanRefreshBase> zoneMap=null;

    /**
     * @param zoneName
     * @param secret
     * @param expiredSecond
     * @param remainSecondRefresh
     */
    private JWTCanRefreshBase(String zoneName,String secret, int expiredSecond, int remainSecondRefresh) {
        this.zoneName=zoneName;
        this.secret = secret;
        this.expiredSecond = expiredSecond;
        this.remainSecondRefresh = remainSecondRefresh;
    }




    /**
     * 创建  JWTBasic 分区  实列
     * @param zoneName 区域名
     * @param secret    私钥
     * @param expiredSecond 过期时间 秒
     * @param remainSecondRefresh 剩余多少秒后刷新
     */
    public static synchronized void newInstance(String zoneName,String secret,int expiredSecond,int remainSecondRefresh){
        if(zoneMap==null) {
            zoneMap=new HashMap<>();
        }
        if(zoneMap.containsKey(zoneName)){
            throw  new RuntimeException("JWTBasic 已经创建实列，请用getZoneInstance 获取");
        }

        if(expiredSecond<=remainSecondRefresh){
            throw  new RuntimeException("剩余多少秒后刷新不能大于等于过期时间");

        }

        zoneMap.put(zoneName,new JWTCanRefreshBase(zoneName,secret, expiredSecond,remainSecondRefresh));
    }


    /**
     * 获取JWTBasic分区实列
     * @param zoneName 区域名
     * @return
     */
    public static JWTCanRefreshBase getInstance(String zoneName){

        if(zoneMap==null){
            throw new RuntimeException("不存在"+zoneName+",JWT区域");
        }

        if(!zoneMap.containsKey(zoneName)){
            throw new RuntimeException("不存在"+zoneName+",JWT区域");
        }

        return  zoneMap.get(zoneName);
    }


    /**
     * 获取 JWT 区域 token
     * @param map
     * @return
     */
    public String getToken(Map<String, String> map) {
        return JWTUtil.getToken(map, expiredSecond,secret);
    }


    /**
     * 验证刷新
     * @param token
     * @return
     */
    public JWTDecoderNewTokenBase verify(String token) {
        //验证
        DecodedJWT verify = JWTUtil.verify(token, secret);

        Map<String,String> claimMap=new HashMap<>();
        verify.getClaims().forEach((k,v)->{
            claimMap.put(k,v.asString());
        });

        int expTime =(int) verify.getExpiresAt().getTime()/1000;
        int nowTime =(int) System.currentTimeMillis()/1000;

        //计算时差（秒）
        int timeDifference=expTime-nowTime;
        System.out.println("时差"+timeDifference);


        if(timeDifference>remainSecondRefresh){
            return new JWTDecoderNewTokenBase(verify,claimMap,token,false);
        }

        synchronized (token){
            //刷新token
            token = JWTUtil.getToken(claimMap, expiredSecond, secret);
            verify = JWTUtil.verify(token, secret);
            return new JWTDecoderNewTokenBase(verify,claimMap,token,true);
        }

    }

    public String getZoneName() {
        return zoneName;
    }

    public String getSecret() {
        return secret;
    }

    public int getExpiredSecond() {
        return expiredSecond;
    }

    public int getRemainSecondRefresh() {
        return remainSecondRefresh;
    }
}
