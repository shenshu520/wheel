package com.wheel.utils.jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.wheel.utils.jwt.entity.JWTExpiredEarlyWarn;

import java.util.Calendar;
import java.util.Map;

public class JWTUtil {

    private JWTUtil(){

    }

    /**
     * 获取token
     * @param map 数据
     * @param expiredSecond 过期时间秒
     * @param secret 秘钥
     * @return
     */
    public static String getToken(Map<String, String> map,int expiredSecond,String secret) {
        Calendar instance = Calendar.getInstance();
        instance.add(Calendar.SECOND, expiredSecond);
        JWTCreator.Builder builder = JWT.create();
        map.forEach((k, v) -> {
            builder.withClaim(k, v);
        });
        String token = builder.withExpiresAt(instance.getTime()).sign(Algorithm.HMAC256(secret));

        return token;
    }

    /**
     * 验证token
     * @param token
     * @param secret 秘钥
     * @return
     */
    public static DecodedJWT verify(String token,String secret) {
        DecodedJWT verify = JWT.require(Algorithm.HMAC256(secret)).build().verify(token);
        return verify;
    }

    /**
     * 验证token
     * @param token
     * @param secret
     * @param expiredEarlyWarnSecret 过期预警秒（提前多少秒过期预警）
     */
    public static JWTExpiredEarlyWarn verify(String token,String secret,int expiredEarlyWarnSecret){
        JWTExpiredEarlyWarn ret=new JWTExpiredEarlyWarn();

        DecodedJWT decodedJWT = JWT.require(Algorithm.HMAC256(secret)).build().verify(token);
        ret.setDecodedJWT(decodedJWT);

        long expTime = decodedJWT.getExpiresAt().getTime();
        long nowTime = System.currentTimeMillis();

        long diffTime = expTime - nowTime;
        int i = expiredEarlyWarnSecret * 1000;
        if(diffTime<i){
            ret.setExpiredEarly(true);
        }
        return ret;
    }
}
