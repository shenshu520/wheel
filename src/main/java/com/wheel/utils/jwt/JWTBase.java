package com.wheel.utils.jwt;

import com.auth0.jwt.interfaces.DecodedJWT;
import com.wheel.utils.jwt.entity.JWTDecoderBase;

import java.util.HashMap;
import java.util.Map;

public class JWTBase {
    /**
     * JWT区域名
     */
    private String zoneName;

    /**
     * 秘钥
     */
    private String secret = null;

    /**
     * 过期时间 秒
     */
    private int expiredSecond = 0;

    /**
     * JWT 区域 Map
     */
    private static Map<String, JWTBase> zoneMap=null;

    public JWTBase(String zoneName,String secret, int expiredSecond) {
        this.zoneName = zoneName;
        this.secret=secret;
        this.expiredSecond=expiredSecond;
    }


    /**
     * 创建  JWTBasic 分区  实列
     * @param zoneName 区域名
     * @param secret    私钥
     * @param expiredSecond 过期时间 秒
     */
    public static synchronized void newInstance(String zoneName,String secret,int expiredSecond){
        if(zoneMap==null) {
            zoneMap=new HashMap<>();
        }
        if(zoneMap.containsKey(zoneName)){
            throw  new RuntimeException("JWTBasic 已经创建实列，请用getZoneInstance 获取");
        }
        zoneMap.put(zoneName,new JWTBase(zoneName,secret, expiredSecond));
    }

    /**
     * 获取JWTBasic分区实列
     * @param zoneName 区域名
     * @return
     */
    public static JWTBase getInstance(String zoneName){

        if(zoneMap==null){
            throw new RuntimeException("不存在"+zoneName+",JWT区域");
        }

        if(!zoneMap.containsKey(zoneName)){
            throw new RuntimeException("不存在"+zoneName+",JWT区域");
        }

        return  zoneMap.get(zoneName);
    }

    /**
     * 获取 JWT 区域 token
     * @param map
     * @return
     */
    public String getToken(Map<String, String> map) {
        return JWTUtil.getToken(map, expiredSecond,secret);
    }


    /**
     * 验证 JWT 区域 token
     * @param token
     * @return
     */
    public JWTDecoderBase verify(String token) {
        DecodedJWT verify =JWTUtil.verify(token,secret);
        Map<String,String> claimsMap=new HashMap<>();
        verify.getClaims().forEach((k,v)->{
            claimsMap.put(k,v.asString());
        });
        JWTDecoderBase jwtDecoderBase=new JWTDecoderBase(verify,claimsMap);
        return jwtDecoderBase;
    }


}
