package com.wheel.utils.jwt.entity;


import com.auth0.jwt.interfaces.DecodedJWT;

import java.util.Map;

public class JWTDecoderNewTokenBase extends JWTDecoderBase{

    /**
     * 新token
     */
    private String token;

    /**
     * 是否token刷新
     */
    private boolean isTokenRefresh;

    public JWTDecoderNewTokenBase(DecodedJWT decodedJWT, Map<String, String> claimsMap,String token,boolean isTokenRefresh) {
        super(decodedJWT, claimsMap);
        this.token=token;
        this.isTokenRefresh=isTokenRefresh;
    }

    public String getToken() {
        return token;
    }

    public boolean isTokenRefresh() {
        return isTokenRefresh;
    }
}
