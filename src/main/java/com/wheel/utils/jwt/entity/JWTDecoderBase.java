package com.wheel.utils.jwt.entity;


import com.auth0.jwt.interfaces.DecodedJWT;

import java.util.Map;

public class JWTDecoderBase  {

    private DecodedJWT decodedJWT;
    private Map<String,String> claimsMap;


    public JWTDecoderBase(DecodedJWT decodedJWT,Map<String,String> claimsMap) {
        this.decodedJWT = decodedJWT;
        this.claimsMap = claimsMap;

    }

    public DecodedJWT getDecodedJWT() {
        return decodedJWT;
    }

    public Map<String,String> getClaimMap(){
        return  claimsMap;
    }

}
