package com.wheel.utils.jwt.entity;

import com.auth0.jwt.interfaces.DecodedJWT;


public class JWTExpiredEarlyWarn {

    /**
     * token数据
     */
    private DecodedJWT decodedJWT;

    /**
     * 过期预警
     * true 快过期
     * false 未过期
     */
    private boolean isExpiredEarly;

    public DecodedJWT getDecodedJWT() {
        return decodedJWT;
    }

    public void setDecodedJWT(DecodedJWT decodedJWT) {
        this.decodedJWT = decodedJWT;
    }

    public boolean isExpiredEarly() {
        return isExpiredEarly;
    }

    public void setExpiredEarly(boolean expiredEarly) {
        isExpiredEarly = expiredEarly;
    }
}
