package com.wheel.custom.exception;

/**
 * @author shenshu
 * @version 1.0
 * @description: 系统异常
 * @E-mail 254602848@qq.com
 * @date 2021/9/3 16:28
 */
public class SysException extends BaseException {

    public SysException(String msg, String code) {
        super(msg, code);
    }

}
