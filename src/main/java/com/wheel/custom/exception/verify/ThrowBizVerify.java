package com.wheel.custom.exception.verify;

import com.wheel.custom.exception.BizException;

public class ThrowBizVerify extends ThrowVerify{

    private static ThrowBizVerify throwBizVerify;

    private String defCode;

    private ThrowBizVerify(String defCode) {
        this.defCode = defCode;
    }

    @Override
    public void throwMsgException(String msg) {
        throw new BizException(msg,defCode);
    }

    @Override
    public void throwException(String code, String msg) {
        throw new BizException(msg,code);
    }

    public static synchronized void newInstance(String defCode){
        if(throwBizVerify!=null){
            throw new RuntimeException("已经存在实列请使用getInstance()");
        }
        throwBizVerify=new ThrowBizVerify(defCode);
    }


    public static ThrowBizVerify getInstance(){
        if(throwBizVerify==null){
            throw new RuntimeException("不存在实列请使用newInstance()");
        }
        return throwBizVerify;
    }

}
