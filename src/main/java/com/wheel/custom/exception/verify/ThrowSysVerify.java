package com.wheel.custom.exception.verify;

import com.wheel.custom.exception.BizException;

public class ThrowSysVerify extends ThrowVerify{

    private static ThrowSysVerify throwSysVerify;

    private String defCode;

    private ThrowSysVerify(String defCode) {
        this.defCode = defCode;
    }

    @Override
    public void throwMsgException(String msg) {
        throw new BizException(msg,defCode);
    }

    @Override
    public void throwException(String code, String msg) {
        throw new BizException(msg,code);
    }

    public static synchronized void newInstance(String defCode){
        if(throwSysVerify!=null){
            throw new RuntimeException("已经存在实列请使用getInstance()");
        }
        throwSysVerify=new ThrowSysVerify(defCode);
    }


    public static ThrowSysVerify getInstance(){
        if(throwSysVerify==null){
            throw new RuntimeException("不存在实列请使用newInstance()");
        }
        return throwSysVerify;
    }

}
