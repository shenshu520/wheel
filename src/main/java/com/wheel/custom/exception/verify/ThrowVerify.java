package com.wheel.custom.exception.verify;

import org.apache.commons.lang3.StringUtils;

public abstract class ThrowVerify{

    public abstract void throwMsgException(String msg);

    public abstract void throwException(String code,String msg);

    /**
     * 空抛异常
     * @param value 数据
     * @param msg   异常消息
     */
    public void  emptyThrowException(String value,String msg){
        if( StringUtils.isEmpty(value)){
            throwMsgException(msg);
        }
    }

    /**
     * true抛异常
     * @param bla  布尔   true抛异常
     * @param msg
     */
    public void trueThrowException(boolean bla, String msg){
        if(bla){
            throwMsgException(msg);
        }
    }


    /**
     * true抛异常
     * @param bla   布尔   true抛异常
     * @param code
     * @param msg
     */
    public void trueThrowException(boolean bla, String code, String msg){
        if(bla){
            throwException(code,msg);
        }
    }

    /**
     * null抛异常
     * @param data
     * @param msg
     */
    public void  nullThrowException(Object data,String msg){
        if(data==null){
            throwMsgException(msg);
        }
    }

    /**
     * null抛异常
     * @param data
     * @param code
     * @param msg
     */
    public void  nullThrowException(Object data,String code,String msg){
        if(data==null){
            throwException(code,msg);
        }
    }
}
