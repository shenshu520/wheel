package com.wheel.custom.exception;

/**
 * @author shenshu
 * @version 1.0
 * @description: 自定义异常
 * @E-mail 254602848@qq.com
 * @date 2021/9/3 16:23
 */
public class BaseException extends  RuntimeException{



    public BaseException(String msg, String code) {
        super(msg);
        this.msg = msg;
        this.code = code;
    }

    /**
     * @description: 异常消息
     */
    private String msg;


    /**
     * @description: 异常代码
     */
    private String code;


    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
