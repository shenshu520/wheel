package com.wheel.custom.result;

/**
 * @author shenshu
 * @version 1.0
 * @description: 结果
 * @E-mail 254602848@qq.com
 * @date 2021/9/3 16:30
 */
public class Result {

    /**
     * @description: 系统异常CODE
     */
    public static  final  String CODE_ERR="500";


    /**
     * @description: 业务异常CODE
     */
    public static  final  String CODE_FAIL="1000";

    /**
     * @description: 请求成功CODE
     */
    public static  final  String CODE_SUCC = "0";


    public static final Result SUCC = newMsgSucc("操作成功");

    public static final Result SUCC_SUBMIT = newMsgSucc("提交成功");

    public static final Result SUCC_SAVE = newMsgSucc("保存成功");

    public static final Result SUCC_MODIFY = newMsgSucc("修改成功");

    public static final Result SUCC_REMOVE = newMsgSucc("移除成功");

    public static final Result ERR = new Result("系统异常",null,CODE_ERR,ResultStateE.ERR);





    /**
     * @description: 结果消息
     */
    private String msg;

    /**
     * @description: 结果数据
     */
    private Object data;

    /**
     * @description: 消息码
     */
    private String code;

    /**
     * @description: 结果状态
     */
    private ResultStateE state;

    public Result(String msg, Object data, String code, ResultStateE state) {
        this.msg = msg;
        this.data = data;
        this.code = code;
        this.state = state;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public ResultStateE getState() {
        return state;
    }

    public void setState(ResultStateE state) {
        this.state = state;
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


    public static Result newMsgSucc(String msg){
        return  new Result(msg,null,CODE_SUCC,ResultStateE.SUCC);
    }

    public static Result newDataSucc(Object data){
        return  new Result(null,data,CODE_SUCC,ResultStateE.SUCC);
    }


    public static Result newSucc(String msg,Object data){
        return  new Result(msg,data,CODE_SUCC,ResultStateE.SUCC);
    }


    public static Result newFail(String msg){
        return  new Result(msg,null,CODE_FAIL,ResultStateE.FAIL);
    }



}
