package com.wheel.custom.result;

/**
 * @author shenshu
 * @version 1.0
 * @description: 结果状态枚举
 * @E-mail 254602848@qq.com
 * @date 2021/9/3 16:37
 */
public enum ResultStateE {
    SUCC,//成功
    FAIL,//失败
    ERR,//错误
}
