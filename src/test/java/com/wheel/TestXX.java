package com.wheel;

import com.wheel.custom.exception.verify.ThrowBizVerify;
import com.wheel.custom.exception.verify.ThrowSysVerify;
import com.wheel.data.ListData;
import com.wheel.entity.UserTestEntity;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author shenshu
 * @version 1.0
 * @description: Test
 * @E-mail 254602848@qq.com
 * @date 2021/9/6 10:29
 */
public class TestXX {

    List<UserTestEntity> userList = ListData.userList;

    @Test
    public void t1() {

        Optional<Integer> max = userList.stream().
                map((e) -> {
                    System.out.println("获取"+e.getAge());
                    return e.getAge();
                }).
                max((a, b) -> {
                    System.out.println("比较"+"==="+a+","+b);
                    return a.compareTo(b);
                });

        max.ifPresent(System.out::println);



        System.out.println("--------------------------------------------");
        List<String> strings = Arrays.asList("abc", "", "bc", "efg", "abcd","", "jkl");
        List<String> filtered = strings.stream().filter(string -> !string.isEmpty()).collect(Collectors.toList());
        System.out.println(filtered);


    }
    @Test
    public void t2() {
        ThrowBizVerify.newInstance("400");
        ThrowSysVerify.newInstance("500");
//        ThrowBizVerify.getInstance().emptyThrowException("","cc");



    }


}
