package com.wheel.data;

import com.wheel.entity.UserTestEntity;


import java.util.ArrayList;
import java.util.List;

/**
 * @author shenshu
 * @version 1.0
 * @description: TODO11
 * @date 2021/9/3 12:04
 */
public class ListData {

    public static List<UserTestEntity> userList = new ArrayList<>();

    static {

        userList.add(new UserTestEntity(1, "王A",31, true));
        userList.add(new UserTestEntity(2, "王B", 21,true));
        userList.add(new UserTestEntity(3, "王C", 41,true));
        userList.add(new UserTestEntity(4, "李A",12, true));
        userList.add(new UserTestEntity(5, "李B",13, true));
        userList.add(new UserTestEntity(6, "李c",21, true));
        userList.add(new UserTestEntity(7, "张A", 25,true));
        userList.add(new UserTestEntity(8, "张B", 31,true));
        userList.add(new UserTestEntity(9, "张C", 41,true));
    }
}
