package com.wheel.data;

import com.wheel.entity.SiteTestEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author shenshu
 * @version 1.0
 * @description: TODO11
 * @date 2021/9/3 13:23
 */
public class TreeData {

    public static  List<SiteTestEntity> list=new ArrayList<>();
    static {
        list.add(new SiteTestEntity(1,0,"宇宙"));
        list.add(new SiteTestEntity(10,1,"银河系"));
        list.add(new SiteTestEntity(20,1,"XX系"));
        list.add(new SiteTestEntity(30,1,"BB系"));

        list.add(new SiteTestEntity(100,10,"太阳系"));
        list.add(new SiteTestEntity(200,1,"XX阳系"));

        list.add(new SiteTestEntity(1000,100,"A星"));
        list.add(new SiteTestEntity(2000,100,"B星"));
        list.add(new SiteTestEntity(3000,100,"C星"));
        list.add(new SiteTestEntity(4000,100,"地球"));

        list.add(new SiteTestEntity(5000,200,"XX阳系-A星"));
        list.add(new SiteTestEntity(5000,200,"XX阳系-B星"));


        list.add(new SiteTestEntity(2,0,"AA宇宙"));
        list.add(new SiteTestEntity(2,0,"BB宇宙"));

    }
}
