package com.wheel.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author shenshu
 * @version 1.0
 * @description: TODO11
 * @date 2021/9/3 13:24
 */
public class SiteTestEntity  {

    public SiteTestEntity(Integer id, Integer pId, String name, List<SiteTestEntity> sonList) {
        this.id = id;
        this.pId = pId;
        this.name = name;
        this.sonList = sonList;
    }

    public SiteTestEntity(Integer id, Integer pId, String name) {
        this.id = id;
        this.pId = pId;
        this.name = name;
        this.sonList = new ArrayList<>();
    }


    private Integer id;

    private Integer pId;

    private String name;

    private List<SiteTestEntity> sonList;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getpId() {
        return pId;
    }

    public void setpId(Integer pId) {
        this.pId = pId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<SiteTestEntity> getSonList() {
        return sonList;
    }

    public void setSonList(List<SiteTestEntity> sonList) {
        this.sonList = sonList;
    }


    @Override
    public String toString() {
        return "SiteTestEntity{" +
                "id=" + id +
                ", pId=" + pId +
                ", name='" + name + '\'' +
                ", sonList=" + sonList +
                '}';
    }
}
