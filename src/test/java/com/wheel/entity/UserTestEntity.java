package com.wheel.entity;

/**
 * @author shenshu
 * @version 1.0
 * @description: TODO11
 * @date 2021/9/3 11:50
 */
public class UserTestEntity {

    /** 
     * @description:
     * @author shenshu
     * @date: 2021/9/3 12:14 
     * @param id ID
     * @param name 名字
     * @param age 年龄
     * @param isBeing 是否活着
     * @return: 
     */ 
    public UserTestEntity(Integer id, String name, Integer age, Boolean isBeing) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.isBeing = isBeing;
    }

    /**
     * @description: ID
     */
    private Integer id;

    /**
     * @description: 用户名
     */
    private String name;

    /**
     * @description: 年龄
     */
    private Integer age;

    /**
     * @description: 是否活着
     */
    private Boolean isBeing;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getBeing() {
        return isBeing;
    }

    public void setBeing(Boolean being) {
        isBeing = being;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }


    @Override
    public String toString() {
        return "UserTestEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", isBeing=" + isBeing +
                '}';
    }
}
