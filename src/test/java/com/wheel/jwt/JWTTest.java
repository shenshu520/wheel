package com.wheel.jwt;

import com.alibaba.fastjson.JSONObject;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.wheel.utils.jwt.JWTBase;
import com.wheel.utils.jwt.JWTCanRefreshBase;
import com.wheel.utils.jwt.entity.JWTDecoderBase;
import com.wheel.utils.jwt.entity.JWTDecoderNewTokenBase;
import org.junit.Test;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class JWTTest {
    public static void main(String[] args) throws  Exception{

        test();
//        test2();
//        test3();
//        test4();
    }

    /**
     * 单个使用
     */
    public static void test(){
        String zoneName="aa";


        JWTBase.newInstance(zoneName,"aslkdwiue",1);

        JWTBase jwt = JWTBase.getInstance(zoneName);

        Map<String, String> data=new HashMap<>();
        data.put("userId","11");
        data.put("userName","傻傻的");
        data.put("yyj","ppp");
        data.put("img","https://img2.baidu.com/it/u=1839565560,3914825985&fm=253&fmt=auto&app=138&f=JPEG?w=640&h=480");
        String token = jwt.getToken(data);
        System.out.println(token);
        System.out.println("token长度："+token.length());


        JWTDecoderBase jd = jwt.verify(token);
        Map<String, String> claimMap = jd.getClaimMap();
        String userId = claimMap.get("userId");
        String userName = claimMap.get("userName");
        String img = claimMap.get("img");
        System.out.println(userId);
        System.out.println(userName);
        System.out.println(img);
    }


    /**
     * 需要多个JWT的使用
     */
    public static void test2(){
        String zoneName="bb";
        JWTBase.newInstance(zoneName,"oisuoui",1);
        JWTBase jwt = JWTBase.getInstance(zoneName);


        Map<String, String> data=new HashMap<>();
        data.put("userId","222");
        data.put("userName","史蒂夫");
        String token = jwt.getToken(data);
        System.out.println(token);


        JWTDecoderBase JD = jwt.verify(token);
        DecodedJWT verify = JD.getDecodedJWT();
        String userId = verify.getClaim("userId").toString();
        String userName = verify.getClaim("userName").toString();
        System.out.println(userId);
        System.out.println(userName);
    }

    /**
     * 性能测试
     */
    public static void test3(){
        String zoneName="cc";
        Integer num=10000;

        JWTBase.newInstance(zoneName,"oisuoui",10);
        JWTBase jwt = JWTBase.getInstance(zoneName);


        long l = System.currentTimeMillis();
        for (int i = 0; i < num; i++) {

            Map<String, String> data=new HashMap<>();
            data.put("userId","222"+i);
            data.put("userName","史蒂夫"+i);

            String token = jwt.getToken(data);
            jwt.verify(token);
        }
        long l2 = System.currentTimeMillis();
        System.out.println(num+"个加密解密，耗时毫秒"+(l2-l));

    }

    /**
     * 可刷新 token 演示
     */
    public static  void test4() throws  Exception{

        String zoneName="raa";
        Integer num=20;

        JWTCanRefreshBase.newInstance(zoneName,"asd",10,2);
        JWTCanRefreshBase jwt = JWTCanRefreshBase.getInstance("raa");
        System.out.println("zoneName="+jwt.getZoneName());


        Map<String, String> data=new HashMap<>();
        data.put("userId","ccc");
        data.put("userName","狗屎");
        String token = jwt.getToken(data);

        for (int i = 0; i < num; i++) {
            Thread.sleep(1000);
            JWTDecoderNewTokenBase verify = jwt.verify(token);
            token=verify.getToken();
            System.out.println("是否刷新："+verify.isTokenRefresh()+"，Token："+verify.getToken());

        }
    }

    @Test
    public void testGenerateToken() {
        // 指定token过期时间为10秒
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.SECOND, 10);
        String token = JWT.create()
                .withHeader(new HashMap<>())  // Header
                .withClaim("userId", 21)  // Payload
                .withClaim("userName", "baobao")
                .withExpiresAt(calendar.getTime())  // 过期时间
                .sign(Algorithm.HMAC256("!34ADAS"));  // 签名用的secret

        System.out.println(token);

        JWTVerifier jwtVerifier = JWT.require(Algorithm.HMAC256("!34ADAS")).build();
        DecodedJWT verify = jwtVerifier.verify(token);
        Claim userId = verify.getClaims().get("userId");
        String s = userId.toString();
        String payload = verify.getPayload();
        System.out.println(payload);
        System.out.println(JSONObject.toJSON(verify).toString());
    }
}
