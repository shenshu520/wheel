package com.wheel.utils.tree;

import com.alibaba.fastjson.JSONObject;
import com.wheel.data.TreeData;
import com.wheel.entity.SiteTestEntity;
import org.junit.Test;

import java.util.List;

/**
 * @author shenshu
 * @version 1.0
 * @description: TODO11
 * @date 2021/9/3 12:56
 */
public class TreeUtilTest {


    @Test
    public void toTree() {
        List<SiteTestEntity> list = TreeData.list;

        List<SiteTestEntity> tree = TreeUtil.toTree(
                list,
                0,
                (e)->e.getId(),
                (SiteTestEntity e)->e.getpId(),
                SiteTestEntity::getSonList
        );

        System.out.println(JSONObject.toJSON(tree));
    }
}
