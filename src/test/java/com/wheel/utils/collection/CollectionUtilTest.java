package com.wheel.utils.collection;

import com.wheel.data.ListData;
import com.wheel.entity.UserTestEntity;
import org.junit.Test;

import java.util.*;

/**
 * @author shenshu
 * @version 1.0
 * @description: TODO11
 * @date 2021/9/3 11:47
 */
public class CollectionUtilTest {



    @Test
    public void toPropertyCollection() {
        List<UserTestEntity> userList = ListData.userList;
        Collection<String> integers = CollectionUtil.toProperty(userList, UserTestEntity::getName);
        System.out.println(integers);
    }



    @Test
    public void toMap() {
        List<UserTestEntity> userList = ListData.userList;
        System.out.println("--------------------");
        Map<Integer, UserTestEntity> map = CollectionUtil.toMap(userList, UserTestEntity::getId);
        for (Map.Entry<Integer, UserTestEntity> entry : map.entrySet()) {
            System.out.println(entry.getKey() + "====" + entry.getValue());
        }
    }


    @Test
    public void toMapList() {
        List<UserTestEntity> userList = ListData.userList;
        Map<Integer, List<UserTestEntity>> map = CollectionUtil.toMapList(userList, UserTestEntity::getAge);

        for (Map.Entry<Integer, List<UserTestEntity>> entry : map.entrySet()) {
            System.out.println(entry.getKey() + "====" + entry.getValue());

        }
    }


    @Test
    public void filterArrayList() {
        List<UserTestEntity> userList = ListData.userList;
        ArrayList<UserTestEntity> filter = (ArrayList<UserTestEntity>) CollectionUtil.filter(userList, (e) -> e.getAge() >= 20);
        System.out.println(filter.getClass()+"=="+filter);

        System.out.println("-------------");

        Set<Integer> set=new HashSet<>(Arrays.asList(1,2,3,4,5,6,7,8,9));
        HashSet<Integer> setRet = (HashSet<Integer>) CollectionUtil.filter(set, e -> e > 5);
        System.out.println(setRet.getClass()+"=="+setRet);

    }




    /**
     * @description: 去重
     * @author shenshu
     * @date: 2021/9/7 16:03
     * @return: void
     */
    @Test
    public void distinct() {

        ArrayList<Integer> data1 = new ArrayList<>(Arrays.asList(1, 2, 2, 3, 3, 2, 4, 12, 41));
        ArrayList<Integer> integers = (ArrayList<Integer>) CollectionUtil.distinct(data1);
        System.out.println(integers);
        System.out.println("----------------------");

        //去重同年龄的
        List<UserTestEntity> userList = ListData.userList;
        ArrayList<UserTestEntity> userTestEntities = (ArrayList<UserTestEntity>) CollectionUtil.distinct(userList, UserTestEntity::getAge);
        System.out.println(userTestEntities);
    }

    @Test
    public void orderBy() {
        List<UserTestEntity> userList = ListData.userList;
        List<UserTestEntity> userTestEntities = CollectionUtil.orderBy(userList, UserTestEntity::getAge, OrderTypeE.ASC);
        System.out.println(userTestEntities);

        System.out.println("----------------------");
        ArrayList<Integer> data2 = new ArrayList<>(Arrays.asList(1, 2, 33, 4, 8, 2, 4, 12, 41));
        List<Integer> asc = CollectionUtil.orderBy(data2, OrderTypeE.ASC);//降序
        System.out.println(asc);


        System.out.println("----------------------");
        List<Integer> desc = CollectionUtil.orderBy(data2, OrderTypeE.DESC);//升序
        System.out.println(desc);

    }


}
